﻿using System;
using System.Text.RegularExpressions;
using ChatSharp.Events;

namespace wormy.Modules
{
    public class PersonalityModule : Module
    {
        public override string Name { get { return "personality"; } }
        public override string Description { get { return "Adds minor things to make the bot more likable."; } }

        public PersonalityModule(NetworkManager network) : base(network)
        {
            var random = new Random();
            
            string[] gratitudeResponses = new[] { "You're welcome!", "Sure thing!", "Any time!", "My pleasure." };
            MatchRegex("^ *thank(s| you),? *" + network.Client.User.Nick + " *$", (PrivateMessageEventArgs e, MatchCollection matches) => 
            {
                Respond(e, gratitudeResponses[random.Next(gratitudeResponses.Length)]);
            }, RegexOptions.Compiled);

            RegisterUserCommand("hug", (args, e) =>
            {
                network.Client.SendAction(string.Format("hugs {0}. It is a slimy experience for us both.", e.PrivateMessage.User.Nick),
                    e.PrivateMessage.Source);
            }, help: "Receive a hug");
        }
    }
}
